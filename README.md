# bookstore with unit tests
Applied methodology of Unit testing

What is Unit Testing?

This is not testing actually. When you are testing the application, you are catching the bugs already been made. When using unit-testing, you are designing the application the way it’s should work and react for exceptions. 
Consider the small workshop producing the chairs. There is three workers - one makes parts of chairs, second assembles them into the chair and third covers chairs with varnish.

Each worker here is unit. When chairs assembler takes right wooden parts, he returns chair. If you will give him the solid wood, he will reject to assemble it into the chair (will throw Exception).

So when opening your own workshop and hiring workers for their job, you think what they are going to do so you can just give them wood and get chairs after their work.

And when you write software you think about functions your software will execute. Instead of giving the workers instructions about what to do in different situations you write functions that will check if your software acts as you planned.

So when you write code, before you apply new unit, you write test for this unit and make test fail. Then you implement unit so test could pass.

This is called Unit Testing. This practice is the part of Test Driven Development.

How to write unit test:

Get the task to implement
Describe how It should work with tests
Create “dirty” functional for test to pass
Improve functional using patterns/algorithms if there is enough time

What it gives you:

Lower “technical debt”
Built-in basic foolproof for user
Less “ah, shap”-bugs (when you forgot about the important part of functional and have to do heavy refactoring)

How to implement unit testing in my project?

Pre-requisites:

Please install Mocha

Please install Chai

Get sure you have mongoldb in localhost

Process:

Clone this repo.

Checkout to no_test_and_func branch

	Watch the video
	
Checkout to test_written branch

	Watch the video
	
Checkout to work_copy branch

	Watch the video
	
Finally you will have basic idea of how to develop through testing that was given to you mechanically. 
Try to implement any small feature using this practice. If you are just starting with unit-tests - don’t bother about test cases. 
Just write one test that is checking for positive case - if your unit works correctly.




